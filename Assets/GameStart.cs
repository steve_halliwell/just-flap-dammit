﻿using UnityEngine;
using System.Collections;

public class GameStart : MonoBehaviour {


	public GameObject[] toggleActivationToStart;
	
	// Update is called once per frame
	void Update () {

		if(Input.GetMouseButtonUp(0))
		{
			foreach(GameObject g in toggleActivationToStart)
				g.SetActive(!g.activeInHierarchy);

			DestroyImmediate(this);
		}
	
	}
}
