﻿using UnityEngine;
using System.Collections;

public class ObstacleLogic : MonoBehaviour {

	public Vector3 vel;
	public float ttl;

	private float count = 0;
	// Use this for initialization
	void Start () {
		rigidbody.velocity = vel;
	}

	void FixedUpdate()
	{
		count+= Time.deltaTime;

		if(count > ttl)
			Destroy(gameObject);
	}
}
