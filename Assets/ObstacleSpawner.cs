﻿using UnityEngine;
using System.Collections;

public class ObstacleSpawner : MonoBehaviour {

	public GameObject obsPre;
	public Transform spawnLoc;

	public Vector2 heightRange, rateRange;

	// Use this for initialization
	void Start () {
		ChainingCreateObstacle();
	}
	
	void ChainingCreateObstacle()
	{
		GameObject newObs = (GameObject)Instantiate(obsPre, spawnLoc.position, Quaternion.identity);
		newObs.transform.Translate(0, Random.Range(heightRange.x, heightRange.y), 0);

		Invoke("ChainingCreateObstacle", Random.Range(rateRange.x, rateRange.y));
	}
}
