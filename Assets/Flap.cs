﻿using UnityEngine;
using System.Collections;

public class Flap : MonoBehaviour {

	public float flapUpForce;
	public Vector3 gravityOverride;
	public GameObject gameoverGO;
	public TextMesh readoutTxt;

	public int numFlaps, numObsPassed;

	// Use this for initialization
	void Start () {
		Physics.gravity = gravityOverride;
		
		Time.timeScale = 1;
	}
	
	// Update is called once per frame
	void Update () {

		if(Input.GetMouseButtonDown(0))
		{
			rigidbody.AddForce(0,flapUpForce * Time.deltaTime,0,ForceMode.Impulse);
			numFlaps++;
		}

		if(Time.timeScale == 0 && Input.GetMouseButtonUp(0))
		{
			Application.LoadLevel("core");
		}
	
	}

	void OnCollisionEnter(Collision col)
	{
		if(col.gameObject.tag == "Respawn")
		{
			int bestFlaps = PlayerPrefs.GetInt("BestFlaps",0);
			int bestObs = PlayerPrefs.GetInt("BestObs",0);

			Time.timeScale = 0;
			//enabled = false;
			gameoverGO.SetActive(true);
			readoutTxt.text = "<b>Current run</b> \nObstacles cleared: " + numObsPassed.ToString() + " Flaps: " + numFlaps.ToString() + "\n";

			if(numObsPassed > bestObs || (numObsPassed == bestObs && numFlaps < bestFlaps))
			{
				PlayerPrefs.SetInt("BestFlaps",numFlaps);
				PlayerPrefs.SetInt("BestObs",numObsPassed);

				readoutTxt.text += "\n<b><i>New Personal Best!</i></b>\n";
				readoutTxt.text += "\n<b>Previous Best</b> \nObstacles cleared: " + bestObs.ToString() + " Flaps: " + bestFlaps.ToString();
			}
			else{
				readoutTxt.text += "\n<b>Best run</b> \nObstacles cleared: " + bestObs.ToString() + " Flaps: " + bestFlaps.ToString();
			}
		}
	}

	void OnTriggerExit(Collider col)
	{
		if(col.tag == "Obstacle")
		{
			numObsPassed++;
		}
	}


	void OnGUI()
	{
		GUI.Label( new Rect(10,10,300,100), "Flaps: " + numFlaps.ToString());
		GUI.Label( new Rect(10,30,300,100), "Obstacles cleared: " + numObsPassed.ToString());
	}
}
